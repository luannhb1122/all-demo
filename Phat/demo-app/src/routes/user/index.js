import { h } from 'preact';
import style from './style.css';
import { useState, useRef } from 'preact/hooks';

const DATA = [
    {
        id: 1,
        name: 'le van teo',
        age: 18 
    },
    {
        id: 2,
        name: 'le van ma',
        age: 22
    },
    {
        id: 3,
        name: 'le chi cong',
        age: 50
    },
]

const User = () => {
    // ref của từng input
    const idRef = useRef()
    const nameRef = useRef()
    const ageRef = useRef()

    // biến chưa dữ liệu trong funtion 
    // biến đầu để gọi, biến thứ 2 là để thay đổi
    const [user, setUser] = useState() 
    const [listData, setListData] = useState(DATA)
    const [selectUser, setSelectUser] = useState()

    // gán value vào cái biến user có trường là id
    const onchangeStt = (evt) => {
        const value = evt.target.value
        setUser({ ...user, ...{ id: value } })
    }
    // gán value vào cái biến user có trường là name
    const onchangeName = (evt) => {
        const value = evt.target.value
        setUser({ ...user, ...{ name: value } })
    }
    // gán value vào cái biến user có trường là age
    const onchangeAge = (evt) => {
        const value = evt.target.value
        setUser({ ...user, ...{ age: value } })
    }


    const handeSave = () => {
        // update
        if (selectUser) {
            const result = listData.filter(value => value.id !== selectUser.id) // lọc trong list nếu nó khác id thì nó return data
            let valueUser = selectUser // tạo biến gán selectUser trong {id, name, age}
            valueUser.id = user?.id || selectUser.id 
            valueUser.name = user?.name || selectUser.name
            valueUser.age = user?.age || selectUser.age

            // gán value input thành rỗng
            idRef.current.value = "";
            nameRef.current.value = "";
            ageRef.current.value = "";


            setSelectUser(null)
            setListData([...result, valueUser]) // => [{}, {}, {}]
            return
        }

        //
        idRef.current.value = "";
        nameRef.current.value = "";
        ageRef.current.value = "";
        setListData([...listData, ...user]) // [{},{},{}] => [{},{},{},{}]
    }

    const handleRemove = (id) => {
        const result = listData.filter(value => value.id !== id) 
        setListData(result)
    }

    const handleUpdate = (id) => {
        const resuslt = listData.find(value => value.id === id)
        setSelectUser(resuslt)
    }

    return (
        <div class={style.classname}>
            <div class={style.ND} style={{ flexWrap: 'wrap', display: "flex" }}>
                <table border="1" style={{ width: "70%" }} >
                    <thead>
                        <tr>
                            <th style="text-align:center">STT</th>
                            <th style="text-align:center">Ten</th>
                            <th style="text-align:center">Tuoi</th>
                            <th style="width:150px;text-align:center">Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listData.map(value => {
                            return (
                                <tr>
                                    <td>{value.id}</td>
                                    <td>{value.name}</td>
                                    <td>{value.age}</td>
                                    <td>
                                        <input type="submit" name="delete" class={style.btn} value="Delete" onClick={() => handleRemove(value.id)} />
                                        <input type="submit" name="update" class={style.btn} value="Update" onClick={() => handleUpdate(value.id)} />
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>


                <div class={style.bg} >
                    <h1>them nguoi dung</h1>
                    <div>
                        <label>STT</label>
                        <input ref={idRef} type="text" onChange={onchangeStt} value={selectUser?.id} />


                    </div>
                    <div>
                        <label>ten </label>
                        <input ref={nameRef} type="text" onChange={onchangeName} value={selectUser?.name} />


                    </div>
                    <div>
                        <label>tuoi</label>
                        <input ref={ageRef} type="text" onChange={onchangeAge} value={selectUser?.age} />


                    </div>
                    <div>

                        <button onClick={handeSave}> {selectUser ? 'Update' : 'Save'}</button>


                    </div>



                </div>


            </div>

        </div>
    )
};

export default User;
