const http = require('http');
const fs = require('fs');
const url = require('url');

function renderHTML(path, res) {
    fs.readFile(path, function(err, data) {
        if (err) {
            res.writeHead(404);
            res.write('File Not Found');
        } else {
            res.write(data);
        };
        res.end();
    });
};

function render404(res) {
    res.writeHead(404);
    res.write('File Not Found');
    res.end();
};


module.exports = {
    Onrequest: function(req, res) {
        const path = url.parse(req.url).pathname;

        switch (path) {
            case '/about':
                renderHTML('./view/about.html', res);
                break;
            case '/':
                renderHTML('./view/home.html', res);
                break;

            default:
                render404(res);
                break;
        }


    }
}