const http = require('http');

const moduleOne = require('./config/libs2')

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end(' < h1 > Hello World < /h1>');
});

server.listen(moduleOne.port, moduleOne.hostname, () => {
    console.log(`Server running at http://${moduleOne.hostname}:${ moduleOne.port}/`);
    moduleOne.show();

});