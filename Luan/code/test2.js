const http = require('http');
const fs = require('fs');


const configs = require('./config/libs3')

function Onrequest(req, res) {
    fs.readFile('./view/home.html', function(err, data) {
        if (err) {
            res.writeHead(404);
            res.write('File Not Found');
        } else {
            res.write(data);
        };

        res.end();
    });

}
server = http.createServer(Onrequest).listen(configs.port, configs.hostname, () => {
    console.log(`Server running at http://${configs.hostname}:${ configs.port}/`);
    configs.show();
});