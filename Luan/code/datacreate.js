// var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
var prod = {
    "_id": "3",
    "Name": "Name 3",
    "price": 26,
    "note": "note 2"
}

//tao database


//moc database
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("mydb");

    //tao bang trong database <=> collection
    //Create a collection name "products":
    // dbo.createCollection("products", function(err, res) {
    //     if (err) throw err;
    //     console.log("Collection created!");
    //     db.close();
    // });

    //insert vao ban product
    // dbo.collection("products").insertOne(prod, function(err, res) {
    //     if (err) throw err;
    //     console.log(res.ops);
    //     console.log("1 document inserted");

    //     db.close();
    // });
    // update du lieu
    // var myquery = { "Name": "Name 1" }
    // var newvalues = {
    //     $set: {
    //         "_id": "1",
    //         "Name": "Name 1new",
    //         "price": 72222,
    //         "note": "note 13"
    //     }
    // }
    // dbo.collection("products").updateOne(myquery, newvalues, function(err, res) {
    //     if (err) throw err;
    //     console.log("1 document updated");
    //     db.close();
    // });
    // //del du lieu ten Name 2
    var myqueryD = { "_id": "4" }
    dbo.collection("customers").deleteOne(myqueryD, function(err, obj) {
        if (err) throw err;
        console.log("1 document deleted");
        db.close();
    });


    //select one
    // dbo.collection("products").findOne({}, function(err, result) {
    //     if (err) throw err;
    //     console.log(result);
    //     db.close();
    // });
    //select all
    dbo.collection("products").find().toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
    // tim kiem theo ID
    // var query = { "_id": "2" };
    // dbo.collection("products").find(query).toArray(function(err, result) {
    //     if (err) throw err;
    //     console.log(result);
    //     db.close();
    // });

});